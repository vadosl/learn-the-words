import React from 'react';
import s from './Button.module.scss'
const Button = () => {
    return (
        <button className={s.button}>
            Начать урок
        </button>
    );
};

export default Button;